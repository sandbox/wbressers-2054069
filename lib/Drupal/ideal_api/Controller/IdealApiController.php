<?php

/**
 * @file
 * Contains \Drupal\ideal_api\Controller\IdealApiController.
 */

namespace Drupal\ideal_api\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Controller routines for iDEAL API routes.
 */
class IdealApiController implements ContainerInjectionInterface {

  public static function create(ContainerInterface $container) {
    return new static($container->get('module_handler'));
  }

  /**
   * This will return the output of the ideal_api page.
   */
  public function ideal_api_page() {
    return array(
      '#markup' => t('This page will be used for testing, and will be removed.'),
    );
  }

}
