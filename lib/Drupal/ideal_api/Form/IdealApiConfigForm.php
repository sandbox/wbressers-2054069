<?php

/**
 * @file
 * Contains \Drupal\ideal_api\Form\IdealApiConfigForm.
 */

namespace Drupal\ideal_api\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Context\ContextInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for iDEAL API admin form.
 */
class IdealApiConfigForm extends ConfigFormBase {

  /**
   * Constructs a \Drupal\ideal_api\Form\IdealApiConfigForm object.
   */
  public function __construct(ConfigFactory $config_factory, ContextInterface $context) {
    parent::__construct($config_factory, $context);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.context.free')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'ideal_api.settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->configFactory->get('ideal_api.settings');

    foreach (language_list() as $key => $language) {
      $language_options[$key] = $language->name;
    }

    // Merchant settings.
    $form['merchant'] = array(
      '#type' => 'fieldset',
      '#title' => t('Merchant'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['merchant']['merchant_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant ID'),
      '#default_value' => $config->get('merchant_id'),
      '#description' => t('The identifier of the merchant.'),
      '#required' => TRUE,
      '#size' => 10,
    );
    $form['merchant']['sub_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Sub ID'),
      '#default_value' => $config->get('sub_id'),
      '#description' => t('The sub identifier of the merchant.'),
      '#required' => TRUE,
      '#size' => 10,
    );

    // Acquirer settings.
    $form['acquirer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Acquirer'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['acquirer']['acquirer_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Acquirer url'),
      '#default_value' => $config->get('acquirer_url'),
      '#description' => t('Enter the url of the acquirer. Change this to the production when testing is complete.'),
      '#required' => TRUE,
    );
    $form['acquirer']['acquirer_timeout'] = array(
      '#type' => 'select',
      '#title' => t('Acquirer timeout'),
      '#default_value' => $config->get('acquirer_timeout'),
      '#description' => t('Connection timeout in seconds.'),
      '#options' => array(
        10 => '10',
        30 => '30',
        60 => '60',
      ),
      '#field_suffix' => t('Seconds'),
    );
    $form['acquirer']['acquirer_expiration'] = array(
      '#type' => 'select',
      '#title' => t('Acquirer expiration period'),
      '#default_value' => $config->get('acquirer_expiration'),
      '#description' => t('Expiration period of the transaction in minutes.'),
      '#options' => array(
        1 => '1',
        5 => '5',
        10 => '10',
      ),
      '#field_suffix' => t('Minutes'),
    );

    // Crypto settings.
    $form['crypto'] = array(
      '#type' => 'fieldset',
      '#title' => t('Crypto'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['crypto']['crypto_private_key_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Private key path'),
      '#default_value' => $config->get('crypto_private_key_path'),
      '#description' => t('Private key file used for signing requests.'),
      '#required' => TRUE,
    );
    $form['crypto']['crypto_private_key_pass'] = array(
      '#type' => 'textfield',
      '#title' => t('Private key password'),
      '#default_value' => $config->get('crypto_private_key_pass'),
      '#description' => t('Password of the private key file.'),
    );
    $form['crypto']['crypto_private_certificate_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Private certificate path'),
      '#default_value' => $config->get('crypto_private_certificate_path'),
      '#description' => t('The filename of the certificate created by the merchant. <strong>NOTE: Store this file outside the Drupal directory</strong>.'),
      '#required' => TRUE,
    );
    $form['crypto']['crypto_acquirer_certificate_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Acquirer certificate path'),
      '#default_value' => $config->get('crypto_acquirer_certificate_path'),
      '#description' => t('The filename of the certificate created by the acquirer. <strong>NOTE: Store this file outside the Drupal directory</strong>.'),
      '#required' => TRUE,
    );

    // Connection settings.
    $form['connection'] = array(
      '#type' => 'fieldset',
      '#title' => t('Connection'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['connection']['connection_proxy'] = array(
      '#type' => 'textfield',
      '#title' => t('Proxy'),
      '#default_value' => $config->get('connection_proxy'),
      '#size' => 10,
    );
    $form['connection']['connection_proxy_acquirer_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Proxy Acquirer Url'),
      '#default_value' => $config->get('connection_proxy_acquirer_url'),
    );

    // Transaction settings.
    $form['transaction'] = array(
      '#type' => 'fieldset',
      '#title' => t('Transaction'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['transaction']['transaction_currency'] = array(
      '#type' => 'select',
      '#title' => t('Currency'),
      '#default_value' => $config->get('transaction_currency'),
      '#options' => array(
        'eur' => 'Euro',
        'usd' => 'Dollar',
      ),
    );
    $form['transaction']['transaction_tax_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Tax rate'),
      '#default_value' => $config->get('transaction_tax_rate'),
      '#description' => t('Specify the tax rate percentage. (must be smaller than 1).'),
      '#required' => TRUE,
      '#field_suffix' => t('%'),
      '#size' => 10,
    );
    $form['transaction']['transaction_language'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#default_value' => $config->get('transaction_language'),
      '#description' => t('Specify the Language code for the transaction.'),
      '#options' => $language_options,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $config = $this->configFactory->get('ideal_api.settings');

    foreach ($form_state['values'] as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
